import subprocess
import shlex
import logging
import os
import json
from collections import OrderedDict
import logging
import fnmatch
import os
import sys

CURRENT_DIRECTORY = os.getcwd()
TEMP_DIR = '/tmp/rmsdifying'

def dumpShaw():
    os.system("rm -rf %s" % TEMP_DIR)
    os.system("mkdir /tmp/rmsdifying")
    with open("/tmp/rmsdifying/dump.tcl","w") as f:
        f.write("""mol new prmtop type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1
mol new ../template.pdb

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/tempdump" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and resid 1 to 52" frame $i]
set sel2 [atomselect 1 "name CA and resid 1 to 52"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix1" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and resid 6 to 16" frame $i]
set sel2 [atomselect 1 "name CA and resid 6 to 16"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix2" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and resid 24 to 34" frame $i]
set sel2 [atomselect 1 "name CA and resid 24 to 34"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix12" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and (resid 24 to 34 or resid 6 to 16)" frame $i]
set sel2 [atomselect 1 "name CA and (resid 24 to 34 or resid 6 to 16)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix3" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and resid 38 to 46" frame $i]
set sel2 [atomselect 1 "name CA and resid 38 to 46"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix13" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and (resid 38 to 46 or resid 6 to 16)" frame $i]
set sel2 [atomselect 1 "name CA and (resid 38 to 46 or resid 6 to 16)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix23" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "name CA and (resid 38 to 46 or resid 24 to 34)" frame $i]
set sel2 [atomselect 1 "name CA and (resid 38 to 46 or resid 24 to 34)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
puts $fo $a
}
close $fo

quit""")
    cmd = "vmd -dispdev text -e /tmp/rmsdifying/dump.tcl"
    logging.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd),shell=False)
    proc.wait()
    os.system('cat /tmp/rmsdifying/tempdump >> /tmp/rmsdifying/all')
    os.system('cat /tmp/rmsdifying/temphelix1 >> /tmp/rmsdifying/helix1')
    os.system('cat /tmp/rmsdifying/temphelix2 >> /tmp/rmsdifying/helix2')
    os.system('cat /tmp/rmsdifying/temphelix12 >> /tmp/rmsdifying/helix12')
    os.system('cat /tmp/rmsdifying/temphelix3 >> /tmp/rmsdifying/helix3')
    os.system('cat /tmp/rmsdifying/temphelix13 >> /tmp/rmsdifying/helix13')
    os.system('cat /tmp/rmsdifying/temphelix23 >> /tmp/rmsdifying/helix23')
    os.system('rm -rf ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])
    os.system('mkdir ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])
    os.system('mv /tmp/rmsdifying/*  ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])

def dumpPDBs(res,resdiff):
    os.chdir(str(res))
    os.system('cp ../template.pdb /tmp/rmsdifying/template.pdb')
    
    if res > 47:
        logging.debug("Running")
        with open("/tmp/rmsdifying/dump.tcl","w") as f:
            f.write("""mol new prmtop.backup type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1
mol new /tmp/rmsdifying/template.pdb


set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/tempdump" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 1 to %(resrmsd)s" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 1 to %(resrmsd)s"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix1" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 6 to 16" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 6 to 16"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix2" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 24 to 34" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 24 to 34"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix12" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and (resid 24 to 34 or resid 6 to 16)" frame $i]
set sel2 [atomselect 1 "protein and name CA and (resid 24 to 34 or resid 6 to 16)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix3" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 38 to 46" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 38 to 46"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix13" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and (resid 38 to 46 or resid 6 to 16)" frame $i]
set sel2 [atomselect 1 "protein and name CA and (resid 38 to 46 or resid 6 to 16)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix23" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and (resid 38 to 46 or resid 24 to 34)" frame $i]
set sel2 [atomselect 1 "protein and name CA and (resid 38 to 46 or resid 24 to 34)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

quit
    """ % {"resrmsd":str(res-1),"res":str(res),"resdiff":str(resdiff)})
        cmd = "vmd -dispdev text -e /tmp/rmsdifying/dump.tcl"
        logging.debug("Running command '" + cmd + "'")
        proc = subprocess.Popen(shlex.split(cmd),shell=False)
        proc.wait()
        os.system('cat /tmp/rmsdifying/tempdump >> /tmp/rmsdifying/all')
        os.system('cat /tmp/rmsdifying/temphelix1 >> /tmp/rmsdifying/helix1')
        os.system('cat /tmp/rmsdifying/temphelix2 >> /tmp/rmsdifying/helix2')
        os.system('cat /tmp/rmsdifying/temphelix12 >> /tmp/rmsdifying/helix12')
        os.system('cat /tmp/rmsdifying/temphelix3 >> /tmp/rmsdifying/helix3')
        os.system('cat /tmp/rmsdifying/temphelix13 >> /tmp/rmsdifying/helix13')
        os.system('cat /tmp/rmsdifying/temphelix23 >> /tmp/rmsdifying/helix23')
    
    elif res > 35:
        logging.debug("Running")
        with open("/tmp/rmsdifying/dump.tcl","w") as f:
            f.write("""mol new prmtop.backup type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1
mol new /tmp/rmsdifying/template.pdb


set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/tempdump" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 1 to %(resrmsd)s" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 1 to %(resrmsd)s"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix1" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 6 to 16" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 6 to 16"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix2" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 24 to 34" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 24 to 34"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix12" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and (resid 24 to 34 or resid 6 to 16)" frame $i]
set sel2 [atomselect 1 "protein and name CA and (resid 24 to 34 or resid 6 to 16)"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

quit
    """ % {"resrmsd":str(res-1),"res":str(res),"resdiff":str(resdiff)})
        cmd = "vmd -dispdev text -e /tmp/rmsdifying/dump.tcl"
        logging.debug("Running command '" + cmd + "'")
        proc = subprocess.Popen(shlex.split(cmd),shell=False)
        proc.wait()
        os.system('cat /tmp/rmsdifying/tempdump >> /tmp/rmsdifying/all')
        os.system('cat /tmp/rmsdifying/temphelix1 >> /tmp/rmsdifying/helix1')
        os.system('cat /tmp/rmsdifying/temphelix2 >> /tmp/rmsdifying/helix2')
        os.system('cat /tmp/rmsdifying/temphelix12 >> /tmp/rmsdifying/helix12')

    elif res > 17:
        logging.debug("Running")
        with open("/tmp/rmsdifying/dump.tcl","w") as f:
            f.write("""mol new prmtop.backup type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1
mol new /tmp/rmsdifying/template.pdb


set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/tempdump" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 1 to %(resrmsd)s" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 1 to %(resrmsd)s"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/temphelix1" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 6 to 16" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 6 to 16"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo

quit
    """ % {"resrmsd":str(res-1),"res":str(res),"resdiff":str(resdiff)})
        cmd = "vmd -dispdev text -e /tmp/rmsdifying/dump.tcl"
        logging.debug("Running command '" + cmd + "'")
        proc = subprocess.Popen(shlex.split(cmd),shell=False)
        proc.wait()
        os.system('cat /tmp/rmsdifying/tempdump >> /tmp/rmsdifying/all')
        os.system('cat /tmp/rmsdifying/temphelix1 >> /tmp/rmsdifying/helix1')

    else:
        logging.debug("Running")
        with open("/tmp/rmsdifying/dump.tcl","w") as f:
            f.write("""mol new prmtop.backup type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1
mol new /tmp/rmsdifying/template.pdb


set nf [molinfo 0 get numframes]
set fo [open "/tmp/rmsdifying/tempdump" "w"]
for {set i 0} {$i < $nf} {incr i} {
set sel1 [atomselect 0 "protein and name CA and resid 1 to %(resrmsd)s" frame $i]
set sel2 [atomselect 1 "protein and name CA and resid 1 to %(resrmsd)s"]
set transformation_matrix [measure fit $sel2 $sel1]
set move_sel [atomselect 1 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fraction [expr "%(res)s.0 + %(resdiff)s.0*$i/$nf"]
puts $fo "$fraction $a"
}
close $fo



quit
    """ % {"resrmsd":str(res-1),"res":str(res),"resdiff":str(resdiff)})
        cmd = "vmd -dispdev text -e /tmp/rmsdifying/dump.tcl"
        logging.debug("Running command '" + cmd + "'")
        proc = subprocess.Popen(shlex.split(cmd),shell=False)
        proc.wait()
        os.system('cat /tmp/rmsdifying/tempdump >> /tmp/rmsdifying/all')

    os.system('rm -rf /tmp/rmsdifying/temp*')
    os.system('rm -rf /tmp/rmsdifying/dump.tcl')
    os.chdir(CURRENT_DIRECTORY)


def run():
    os.system("rm -rf /tmp/rmsdifying")
    os.system("mkdir /tmp/rmsdifying")
    matches = []
    for root, dirnames, filenames in os.walk('./'):
        for filename in fnmatch.filter(filenames, '03_Prod_reimage.mdcrd'):
            matches.append(int(root.split('/')[1]))
    matches = sorted(matches)
    resdiff = matches[1]-matches[0]
    for match in matches:
        print(match)
        try:
            dumpPDBs(match,resdiff)
        except:
            pass
    os.system('rm -rf ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])
    os.system('mkdir ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])
    os.system('mv /tmp/rmsdifying/*  ~/homeodomain-analysis/' + CURRENT_DIRECTORY.split('/')[-1])


if len(sys.argv) > 1:
    if sys.argv[1]=='shaw':
        dumpShaw()
else:
    run()

