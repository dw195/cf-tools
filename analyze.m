i=1;
files{i}.name='homeodomain-cotranslational-last1-temp380-300ns';
files{i}.time = '300';
files{i}.type='last';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-last-1aa-50ns-380';
files{i}.time='50';
files{i}.type='last';
files{i}.temp='380';
files{i}.chop='1';

i=i+1;
files{i}.name='homeodomain-cotranslational-last-1aa-50ns-380-2';
files{i}.time='50';
files{i}.type='last';
files{i}.temp='380';
files{i}.chop='1';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd1';
files{i}.time='100';
files{i}.type='rmsd';
files{i}.temp='360';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd2-temp380';
files{i}.time='100';
files{i}.type='rmsd';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd3-temp340';
files{i}.time='100';
files{i}.type='rmsd';
files{i}.temp='340';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd4-temp400';
files{i}.time='100';
files{i}.type='rmsd';
files{i}.temp='400';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd5-temp380';
files{i}.time='100';
files{i}.type='rmsd';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotranslational-rmsd6-temp380-200ns';
files{i}.time='200';
files{i}.type='rmsd';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotransltional-last';
files{i}.time='100';
files{i}.type='last';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='homeodomain-cotransltional-last2';
files{i}.time='100';
files{i}.type='last';
files{i}.temp='380';
files{i}.chop='4';

i=i+1;
files{i}.name='folding';
files{i}.time='';
files{i}.type='folding';
files{i}.temp='360';
files{i}.chop='';


plots{1}='all';
plots{2}='helix1';
plots{3}='helix2';
plots{4}='helix3';
plots{5}='helix12';
plots{6}='helix13';
plots{7}='helix23';
colors = jet(7);

for j=1:length(plots)
    d{j} = {};
    d{j}{1} = [];
    d{j}{2} = [];
    d{j}{3} = [];
end

for i = 1:length(files)-1
    figure(1)
    for j=1:length(plots)
        try
        a = importdata([files{i}.name '/' plots{j}]);
        plot(a(:,1),a(:,2),'color',colors(j,:))
        xlabel('fractional aa')
        ylabel('RMSD')
        axis([1 60 0 20])
        title(sprintf('%s: %s, %s, %sK, %s aa, %s ns',files{i}.name,plots{j},files{i}.type,files{i}.temp,files{i}.chop,files{i}.time));
%          saveas(gcf,sprintf('%s/%s.png',files{i}.name,plots{j}))
        data = a(a(:,1)>50,2);
        for ij=1:10
            data = [data; data];
        end
        if strcmp(files{i}.type,'last') ==1
            d{j}{1} = [d{j}{1}; randsample(data,1000)];
        else
            d{j}{2} = [d{j}{2}; randsample(data,1000)];
        end
        catch
        end
    end
    figure(2)
    for j=1:length(plots)
        try
        a = importdata([files{i}.name '/' plots{j}]);
        if j==1
            subplot(4,2,1:2)
        else
            subplot(4,2,j+1)
        end
        plot(a(:,1),a(:,2),'color',colors(j,:))
        xlabel('fractional aa')
        ylabel('RMSD')
        if j==1
            subplot(4,2,1:2)
            title(sprintf('%s %s, %sK, %saa, %s ns',files{i}.name,files{i}.type,files{i}.temp,files{i}.chop,files{i}.time),'FontSize',6);
           axis([1 60 0 20])
        else
            subplot(4,2,j+1)
            title(sprintf('%s',plots{j}));
           axis([1 60 0 10])
        end
        catch
        end
    end
    set(gcf, 'Position', get(0,'Screensize')); 
    saveas(gcf,sprintf('%s/full.png',files{i}.name))

end



figure(3)
i=length(files)
for j=1:length(plots)
    try
    a = importdata([files{i}.name '/' plots{j}]);
    plot(a(:,size(a,2)),'color',colors(j,:))
    xlabel('steps')
    ylabel('RMSD')
    axis([0 length(a(:,size(a,2))) 0 20])
    title(sprintf('%s: %s, %s, %sK, %saa',files{i}.name,plots{j},files{i}.type,files{i}.temp,files{i}.chop));
     saveas(gcf,sprintf('%s/%s.png',files{i}.name,plots{j}))
    data = a(:,size(a,2));
    d{j}{3} = [data];
    catch
    end
end
figure(2)
for j=1:length(plots)
    try
    a = importdata([files{i}.name '/' plots{j}]);
    if j==1
        subplot(4,2,1:2)
    else
        subplot(4,2,j+1)
    end
    plot(a(:,size(a,2)),'color',colors(j,:))
    xlabel('steps')
    ylabel('RMSD')
    if j==1
        subplot(4,2,1:2)
        title(sprintf('%s %s, %sK, %saa, %s ns',files{i}.name,files{i}.type,files{i}.temp,files{i}.chop,files{i}.time),'FontSize',6);
       axis([0 length(a(:,size(a,2))) 0 20])
    else
        subplot(4,2,j+1)
        title(sprintf('%s',plots{j}));
       axis([0 length(a(:,size(a,2))) 0 10])
    end
    catch
    end
end
set(gcf, 'Position', get(0,'Screensize')); 
saveas(gcf,sprintf('%s/full.png',files{i}.name))

close all;
for i=1:length(plots)
    if i==1
        subplot(4,2,1:2)
        nhist(d{i},'legend',{'last (after 50 residues)','rmsd (after 50 residues)','folding'},'noerror')
        xlabel('rmsd')
    else
        subplot(4,2,i+1)
        nhist(d{i},'noerror')
        xlabel('rmsd')
    end
    title(sprintf('%s',plots{i}))
end
set(gcf, 'Position', get(0,'Screensize')); 
saveas(gcf,sprintf('type_comparision.png',files{i}.name,plots{j}))